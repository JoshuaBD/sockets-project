package client;

import javax.swing.JButton;
import javax.swing.JDialog;

import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Login extends JDialog {
	private boolean correcte = false;
	private JTextField textUser;
	private JTextField textPass;

	public Login() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setMinimumSize(new Dimension(230, 130));
		setTitle("Login");
		setModalityType(ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(null);
		
		JTextPane txtUsername = new JTextPane();
		txtUsername.setBackground(Color.LIGHT_GRAY);
		txtUsername.setText("Usuario:");
		txtUsername.setEditable(false);
		txtUsername.setBounds(10, 10, 86, 20);
		getContentPane().add(txtUsername);
		
		JTextPane txtPassword = new JTextPane();
		txtPassword.setBackground(Color.LIGHT_GRAY);
		txtPassword.setText("Contrase\u00F1a:");
		txtPassword.setEditable(false);
		txtPassword.setBounds(10, 35, 86, 20);
		getContentPane().add(txtPassword);
		
		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textUser.getText().equals("") && !textPass.getText().equals("")) {
					correcte = true;
					setVisible(false);
				} else {
					correcte = false;
					setVisible(false);
				}
			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(10, 66, 86, 23);
		getContentPane().add(btnAccept);
		
		JButton btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(106, 66, 86, 23);
		getContentPane().add(btnCancel);
		
		textUser = new JTextField();
		textUser.setBounds(106, 10, 108, 20);
		getContentPane().add(textUser);
		textUser.setColumns(10);
		
		textPass = new JTextField();
		textPass.setColumns(10);
		textPass.setBounds(106, 35, 108, 20);
		getContentPane().add(textPass);
		
	}

	public boolean isCorrecte() {
		return correcte;
	}

	public void setCorrecte(boolean correcte) {
		this.correcte = correcte;
	}

	public JTextField getTextUser() {
		return textUser;
	}

	public void setTextUser(JTextField textUser) {
		this.textUser = textUser;
	}

	public JTextField getTextPass() {
		return textPass;
	}

	public void setTextPass(JTextField textPass) {
		this.textPass = textPass;
	}
	
	
}
