package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*; 
import java.net.*; 
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder; 

public class Client extends JFrame
{
	public static String address = "localhost"; //PONER AQUI IP JODER
    public JTextArea areaTexto;
    private JTextField txtDadesPersonals;
	private JTextField txtSubhastaActual;
	private JTextField txtLicitarQuantitatMarcada;
	private JTextField txtLicitacionsActuals;
	private JTextField txtLotsGuanyats;
	public JTextArea dadesPersonalsText;
	public JTextArea subhastaActualText;
	public JTextArea licitacionsText;
	public JTextArea lotsGuanyatsText;
	public JSpinner spinner;
	final static int ServerPort = 1234;
	public static volatile boolean finalize = false;
	public String comanda = "";
	
	public Client(){
        super("Client");
        
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
       
        JMenu menuArchivo = new JMenu("Archivo"); 
        JMenuItem salir = new JMenuItem("Salir");
        JMenuItem login = new JMenuItem("Login");
        menuArchivo.add(login);
        menuArchivo.add(salir);
        
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        barra.add(menuArchivo);
        
        salir.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
        });
        
        login.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	JDialog dialogPanel = new Login();
        		dialogPanel.setLocationRelativeTo(null);
        		dialogPanel.setVisible(true);

        		if (((Login) dialogPanel).isCorrecte()) {
        			String user = ((Login) dialogPanel).getTextUser().getText().toString();
        			String pass = ((Login) dialogPanel).getTextPass().getText().toString();
        			
        			comanda = "/login " + user + " " + pass; 
        			
        		}

        		dialogPanel.dispose();
            }
        });
        
        JPanel panel = new JPanel();
        panel.setBackground(SystemColor.activeCaption);
        panel.setBorder(new LineBorder(Color.BLACK));
        panel.setBounds(0, 0, 251, 89);
        getContentPane().add(panel);
        panel.setLayout(null);
        
        JButton btnLicitar = new JButton("LICITAR");
        btnLicitar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if(!spinner.getValue().toString().equals("0")) {
        			comanda = "/licitar " + spinner.getValue().toString();
            		spinner.setValue(0);
        		}
        	}
        });
        btnLicitar.setBounds(10, 55, 231, 23);
        panel.add(btnLicitar);
        
        txtLicitarQuantitatMarcada = new JTextField();
        txtLicitarQuantitatMarcada.setBackground(SystemColor.activeCaption);
        txtLicitarQuantitatMarcada.setText("Licitar Quantitat Introduida");
        txtLicitarQuantitatMarcada.setBounds(10, 24, 159, 20);
        txtLicitarQuantitatMarcada.setEditable(false);
        panel.add(txtLicitarQuantitatMarcada);
        txtLicitarQuantitatMarcada.setColumns(10);
        
        spinner = new JSpinner();
        spinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
        spinner.setBounds(179, 24, 62, 20);
        panel.add(spinner);
        
        txtDadesPersonals = new JTextField();
        txtDadesPersonals.setBackground(Color.LIGHT_GRAY);
        txtDadesPersonals.setText("Dades Personals");
        txtDadesPersonals.setHorizontalAlignment(SwingConstants.CENTER);
        txtDadesPersonals.setEditable(false);
        txtDadesPersonals.setBorder(new LineBorder(new Color(0, 0, 0)));
        txtDadesPersonals.setBounds(250, 0, 158, 20);
        getContentPane().add(txtDadesPersonals);
        txtDadesPersonals.setColumns(10);
        
        dadesPersonalsText = new JTextArea();
        dadesPersonalsText.setBackground(SystemColor.activeCaption);
        dadesPersonalsText.setBorder(new LineBorder(new Color(0, 0, 0)));
        dadesPersonalsText.setBounds(250, 20, 158, 69);
        dadesPersonalsText.setEditable(false);
        dadesPersonalsText.setFont(new Font("Arial", Font.BOLD, 13));
        getContentPane().add(dadesPersonalsText);
        
        txtSubhastaActual = new JTextField();
        txtSubhastaActual.setBackground(Color.LIGHT_GRAY);
        txtSubhastaActual.setText("Subhasta Actual");
        txtSubhastaActual.setHorizontalAlignment(SwingConstants.CENTER);
        txtSubhastaActual.setEditable(false);
        txtSubhastaActual.setColumns(10);
        txtSubhastaActual.setBorder(new LineBorder(new Color(0, 0, 0)));
        txtSubhastaActual.setBounds(250, 89, 158, 20);
        getContentPane().add(txtSubhastaActual);
        
        subhastaActualText = new JTextArea();
        subhastaActualText.setBackground(SystemColor.activeCaption);
        subhastaActualText.setBorder(new LineBorder(new Color(0, 0, 0)));
        subhastaActualText.setBounds(250, 109, 158, 151);
        subhastaActualText.setEditable(false);
        subhastaActualText.setFont(new Font("Arial", Font.BOLD, 13));
        getContentPane().add(subhastaActualText);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(0, 109, 251, 151);
        scrollPane.setBorder(new LineBorder(new Color(0, 0, 0)));
        
        licitacionsText = new JTextArea();
        licitacionsText.setBackground(SystemColor.activeCaption);
        //licitacionsText.setBounds(0, 109, 251, 151);
        licitacionsText.setEditable(false);
        scrollPane.setViewportView(licitacionsText);
        
        getContentPane().add(scrollPane);
        
        txtLicitacionsActuals = new JTextField();
        txtLicitacionsActuals.setText("Licitacions Actuals");
        txtLicitacionsActuals.setHorizontalAlignment(SwingConstants.CENTER);
        txtLicitacionsActuals.setEditable(false);
        txtLicitacionsActuals.setColumns(10);
        txtLicitacionsActuals.setBorder(new LineBorder(new Color(0, 0, 0)));
        txtLicitacionsActuals.setBackground(Color.LIGHT_GRAY);
        txtLicitacionsActuals.setBounds(0, 89, 251, 20);
        getContentPane().add(txtLicitacionsActuals);
        
        txtLotsGuanyats = new JTextField();
        txtLotsGuanyats.setText("Lots Guanyats");
        txtLotsGuanyats.setHorizontalAlignment(SwingConstants.CENTER);
        txtLotsGuanyats.setEditable(false);
        txtLotsGuanyats.setColumns(10);
        txtLotsGuanyats.setBorder(new LineBorder(new Color(0, 0, 0)));
        txtLotsGuanyats.setBackground(Color.LIGHT_GRAY);
        txtLotsGuanyats.setBounds(408, 0, 158, 20);
        getContentPane().add(txtLotsGuanyats);
        
        lotsGuanyatsText = new JTextArea();
        lotsGuanyatsText.setEditable(false);
        lotsGuanyatsText.setBorder(new LineBorder(new Color(0, 0, 0)));
        lotsGuanyatsText.setBackground(SystemColor.activeCaption);
        lotsGuanyatsText.setBounds(408, 20, 158, 240);
        getContentPane().add(lotsGuanyatsText);
        
        setSize(572, 312);
        setResizable(false);
        setVisible(true);
        
    }
	
    public void mostrarMensaje(String mensaje) {
        areaTexto.append(mensaje + "\n");
    } 
	
	public static void main(String args[]) throws UnknownHostException, IOException 
	{ 
		Scanner scn = new Scanner(System.in); 
		Client client = new Client();
		InetAddress ip = InetAddress.getByName(address); 
		Socket s = new Socket(ip, ServerPort); 
		
		DataInputStream dis = new DataInputStream(s.getInputStream()); 
		DataOutputStream dos = new DataOutputStream(s.getOutputStream()); 
		
		Thread sendMessage = new Thread(new Runnable() 
		{
			@Override
			public void run() { 
				while (!finalize) { 
					if(!client.comanda.equals("")) {
					String[] parts = client.comanda.split(" ");
						try { 
							dos.writeUTF(client.comanda);
							client.comanda = "";
						} catch (IOException e) { 
							e.printStackTrace(); 
						} 
						
					}
				}
			}
		});
 
		Thread readMessage = new Thread(new Runnable() 
		{
			
			@Override
			public void run() { 

				while (!finalize) { 
					try {
						String[] s;
						String mensg = dis.readUTF();
						if(mensg.contains("/saldo;")) {
							s = mensg.split(";");
							client.dadesPersonalsText.setText(s[1]);
						} else if(mensg.contains("/subhasta;")) {
							s = mensg.split(";");
							client.subhastaActualText.setText(s[1]);
						} else if(mensg.contains("/new;")) {
							s = mensg.split(";");
							client.licitacionsText.setText(s[1] + "\n");
						} else if(mensg.contains("/lots")) {
							s = mensg.split(";");
							client.lotsGuanyatsText.setText("idLot \timportAdjud.\n");
							if(s.length >= 2)
								client.lotsGuanyatsText.append(s[1]);
								
						} else {
							client.licitacionsText.append(mensg + "\n");
						}
					} catch (EOFException e2) {
						break;
					} catch (SocketException e3) {
						finalize = true;
						break;
					} catch (IOException e) {
						e.printStackTrace(); 
					}
				}
			} 
		}); 

		sendMessage.start(); 
		readMessage.start();
		
		do {
			
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if(finalize) {
				dis.close();
				dos.close();
			}
				
		} while(!finalize);

	} 
} 
