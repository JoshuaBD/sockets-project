package server;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tastat.DaemonLots;
import tastat.DaemonSubhasta;
import tastat.Subhasta;
import tastat.Timer;

import java.net.*;

public class Server extends JFrame {
	public JTextField campoTexto;
	public JTextArea areaTexto;
	static Vector<ClientHandler> ar = new Vector<>();
	public DB db = new DB();
	public Subhasta subhasta = null;
	public Timer timer;
	public String mensaje;

	public Server() {
		super("Server");

		campoTexto = new JTextField();
		campoTexto.setEditable(true);
		add(campoTexto, BorderLayout.NORTH);

		areaTexto = new JTextArea();
		areaTexto.setEditable(false);
		add(new JScrollPane(areaTexto), BorderLayout.CENTER);
		areaTexto.setBackground(Color.LIGHT_GRAY);
		areaTexto.setForeground(Color.BLACK);
		campoTexto.setForeground(Color.BLACK);

		JMenu menuArchivo = new JMenu("Archivo");
		JMenuItem salir = new JMenuItem("Salir");
		menuArchivo.add(salir);

		JMenuBar barra = new JMenuBar();
		setJMenuBar(barra);
		barra.add(menuArchivo);

		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		setSize(300, 320);
		setVisible(true);

		campoTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				mensaje = event.getActionCommand();
				mostrarMensaje(mensaje);
				campoTexto.setText("");
			}
		});
	}

	public void mostrarMensaje(String mensaje) {
		String[] s;
		String res = "";
		if (mensaje.contains("/licitacions")) {
			res = db.veureLicitacions();
			areaTexto.append("idLicit\t idLot\t idClient\t idImport\t Aceptada\n");
			areaTexto.append(res + "\n");
		} else if (mensaje.contains("/lots")) {
			res = db.veureLots();
			areaTexto.append("idLot\t idProducte\t preuSortida\t importAdjudic\tidClient\n");
			areaTexto.append(res + "\n");
		} else if (mensaje.contains("/usuaris")) {
			for(ClientHandler ch: ar) {
				if(ch.isloggedin) {
					res += ch.name + "\n";
				}
			}
			if(res.equals(""))
				res = "Cap usuari conectat...";
			
			areaTexto.append("Usuaris conectats: \n" + res + "\n");
		} else if (mensaje.contains("/timer")) {
			s = mensaje.split(" ");
			int temps = Integer.parseInt(s[1]);
			Timer.temps = temps;
			areaTexto.append("S'ha canviat el temps d'espera a " + temps + "\n");
		} else if (mensaje.contains("/help")) {
			res = "/licitacions - Mostra Licitacions\n" 
					+ "/lots - Mostra Lots\n"
					+ "/timer NUM - Canvia el temps d'espera de la subhasta (En Segons!)\n"
					+ "/usuaris - Mostra els usuaris conectats \n";
			areaTexto.append(res + "\n");
		}
	}

	public void habilitarTexto(boolean editable) {
		campoTexto.setEditable(editable);
	}

	public static void main(String[] args) throws IOException {
		ServerSocket ss = new ServerSocket(1234);
		Socket s;
		Server server = new Server();

		DaemonLots dl = new DaemonLots(server.db);
		dl.setDaemon(true);
		dl.start();

		DaemonSubhasta ds = new DaemonSubhasta(server, ar);
		ds.setDaemon(true);
		ds.start();

		while (true) {
			s = ss.accept();

			// System.out.println("New client request received: " + s);
			DataInputStream dis = new DataInputStream(s.getInputStream());
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());

			// System.out.println("Creating a new handler for this client...");
			ClientHandler mtch = new ClientHandler(s, "client", dis, dos, server);

			Thread t = new Thread(mtch);
			// System.out.println("Adding this client to active client list");

			ar.add(mtch);
			t.start();
		}
	}

	public void actualitzarSubhasta(String s) {
		timer.interrupt();
		timer = new Timer(this);
		timer.start();

		String infoSub = this.subhasta.getInfo();

		for (ClientHandler ch : ar) {
			if (ch.isloggedin) {
				try {
					ch.dos.writeUTF(s);
					ch.dos.writeUTF("/subhasta;" + infoSub);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
