package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

import tastat.Subhasta;

public class DB {
	private String url = "jdbc:mysql://10.1.6.60/ttb?useLegacyDatetimeCode=false&serverTimezone=UTC";  //10.1.6.60:3306
	private Properties p;
	
	public DB() {
		p = new Properties();
		p.put("user", "admin");
		p.put("password", "super3");
		
		/*p.put("user", "root");
		p.put("password", "");*/
		
	}
	
	public String getUsuaris() {
		String s = "";
		
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM client");
			
			//System.out.println("ENTRA");
			
			s += "TABLA: client" + "\n";
			//System.out.println(s);
			while(rs.next()) {
				s += "USUARIO: " + rs.getString("usuario") + " | ";
				s += "PASSWORD: " + rs.getString("password") + " | ";
				s += "SALDO: " + rs.getInt("saldo") + "\n";
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		return s;
		
	}
	
	public boolean login(String usuario, String contra) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM client WHERE usuario = '" + usuario + "'");
			
			while(rs.next()) {
				//System.out.println(rs.getString("usuario") + " " + rs.getString("password"));
				if(rs.getString("password").equals(contra)) {
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		
		return false;
	}
	
	public String getSaldo(String usuario) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		String resultado = "";
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM client WHERE usuario = '"+ usuario+"'");
			
			while(rs.next()) {
				resultado += rs.getInt("saldo_actual") + ";";
				resultado += rs.getInt("saldo_inicial");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		
		return resultado;
	}
	
	public String getInfo(String usuario) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		String nom = "";
		int saldo = 0;
		int saldoIni = 0;
		String resultado = "";
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM client WHERE usuario = '"+ usuario+"'");
			
			while(rs.next()) {
				nom = rs.getString("nomClient");
				saldo = rs.getInt("saldo_actual");
				saldoIni = rs.getInt("saldo_inicial");
			}
			
			resultado = "Nom: "+nom+"\nSaldo: "+saldo+"\nSaldo Inicial: "+saldoIni;
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		return resultado;
	}
	
	public String getNombreCliente(String usuario) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		String s = "";
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM client WHERE usuario = '" + usuario + "'");
			
			while(rs.next()) {
				s += rs.getString("nomClient");
				//System.out.println(s);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		
		return s;
		
	}
	
	public void initSaldo() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        
        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            st.executeUpdate("UPDATE client SET saldo_actual = saldo_inicial");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
    }

	public Subhasta getNextSubhasta() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        
        Subhasta s = new Subhasta(this); 
        
        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            rs = st.executeQuery("SELECT * FROM lot WHERE idClient IS NULL LIMIT 1");
            while(rs.next()) {
                s.idLot = rs.getInt("idLot");
                s.idProducte = rs.getInt("idProducte");
                s.preuSortida = rs.getInt("preuSortida");
                s.idClient = rs.getInt("idClient");
            }
            
            s.licitacioMax = s.preuSortida;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
        
        return s;
    }


	
	public void generarLot(int idProd) {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			st.executeUpdate("INSERT INTO lot (idProducte, preuSortida, importAdjudicacio, idClient) VALUES ("+idProd+",100,null,null);");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
	}
	
	public void restarSaldo(String usuario, int resta) {
        Connection con = null;
        Statement st = null;
        
        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            st.executeUpdate("UPDATE CLIENT SET saldo_actual = saldo_actual-'" + (resta) + "' WHERE usuario = '" + usuario + "'");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
    }
	
	public int getIdClient(String user) {
		int numeros = 0;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT idClient FROM Client WHERE usuario ='"+user+"'");
			while(rs.next()) {
				numeros = rs.getInt("idClient");
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		return numeros;
	}
	
	public ArrayList getProductes() {
		ArrayList<Integer> productes = new ArrayList<Integer>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT idProducte FROM Producte WHERE tipusProducte = 'VENDIBLE'");
			while(rs.next()) {
				int id = rs.getInt("idProducte");
				//System.out.println(id);
				productes.add(id);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		return productes;
	}
	
	public int getLotClient() {
		int numeros = 0;
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(this.url, this.p);
			st = con.createStatement();
			rs = st.executeQuery("SELECT count(*) 'count' FROM LOT WHERE idClient IS NULL");
			while(rs.next()) {
				numeros = rs.getInt("count");
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
		return numeros;
	}
	
	public void actualitzarSubhasta(int idLot, int idClient, int importAdjudicacio) {
        Connection con = null;
        Statement st = null;

        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            //System.out.println(idLot + " " + idClient + " " + importAdjudicacio);
            st.executeUpdate("UPDATE LOT SET importAdjudicacio = " + importAdjudicacio + ", idClient = " + idClient
                    + " WHERE idLot = " + idLot);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
    }
	
	public void creaLicitacio(int idLot, int idClient, int idImport, boolean acceptada) {
        Connection con = null;
        Statement st = null;

        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            
            int ac = 0;
            if(acceptada)
            	ac = 1;
            else
            	ac = 0;
            
            st.executeUpdate("INSERT INTO licitacions(idLot,idClient,idImport,aceptada) VALUES('" + idLot + "','" + idClient
                    + "','" + idImport + "','" + ac + "')");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}
    }
	
	public String veureLots() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        int idLot = 0, idProducte = 0,preuSortida = 0, importAdjudicacio = 0, idClient = 0;
        String s = "";

        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            rs = st.executeQuery("SELECT * FROM lot");
            
            while(rs.next()) {
                idLot = rs.getInt("idLot");
                idProducte = rs.getInt("idProducte");
                preuSortida = rs.getInt("preuSortida");
                importAdjudicacio = rs.getInt("importAdjudicacio");
                idClient = rs.getInt("idClient");

                s += idLot+"\t"+idProducte+"\t"+preuSortida+"\t"+importAdjudicacio+"\t"+idClient+"\n";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try { rs.close(); } catch (Exception e) { /* ignored */ }
		    try { st.close(); } catch (Exception e) { /* ignored */ }
		    try { con.close(); } catch (Exception e) { /* ignored */ }
		}

        return s;
    }
	
	public String veureLicitacions() {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        int idLicitacio = 0, idLot = 0, idClient = 0, idImport = 0;
        byte aceptada;

        String s = "";

        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            rs = st.executeQuery("SELECT * FROM licitacions");
            while(rs.next()) {
                idLicitacio = rs.getInt("idLicitacio");
                idLot = rs.getInt("idLot");
                idClient = rs.getInt("idClient");
                idImport = rs.getInt("idImport");
                aceptada = rs.getByte("aceptada");

                s = s + idLicitacio+"\t"+idLot+"\t"+idClient+"\t"+idImport+"\t"+aceptada+"\n";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { rs.close(); } catch (Exception e) {  /* ignored / }
            try { st.close(); } catch (Exception e) { / ignored / }
            try { con.close(); } catch (Exception e) { / ignored */ }
        }
        return s;
    }
	
	public String veureGuanyats(int idUser) {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        int idLot = 0, importAdjudicacio = 0;
        String s ="";

        try {
            con = DriverManager.getConnection(this.url, this.p);
            st = con.createStatement();
            rs = st.executeQuery("SELECT * FROM lot WHERE idClient = "+idUser);
            
            while(rs.next()) {
                idLot = rs.getInt("idLot");
                importAdjudicacio = rs.getInt("importAdjudicacio");

                s += idLot+"\t"+importAdjudicacio+"\n";
            }
            
            //System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { rs.close(); } catch (Exception e) {  /* ignored / }
            try { st.close(); } catch (Exception e) { / ignored / }
            try { con.close(); } catch (Exception e) { / ignored */ }
        }

        return s;
    }
	
	
}
