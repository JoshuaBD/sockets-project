package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

public class ClientHandler implements Runnable  
{ 
    Scanner scn = new Scanner(System.in); 
    public String name;
    public String usuario; 
    public final DataInputStream dis; 
    public final DataOutputStream dos;
    public int saldoActual;
    public int saldoInicial;
    public Socket s; 
    public boolean isloggedin;
    public Server server;

    public ClientHandler(Socket s, String name, 
                            DataInputStream dis, DataOutputStream dos, Server server) { 
        this.dis = dis; 
        this.dos = dos; 
        this.name = name; 
        this.s = s; 
        this.isloggedin = false;
        this.server = server;
    } 
  
    @Override
    public void run() { 
  
        String received; 
        while (true)  
        { 
            try
            { 
                received = dis.readUTF(); 
                if(!received.equals("")) {
	                if(!this.isloggedin) {
	                	String[] parts = received.split(" ");
                		//System.out.println(parts[1] + " " + parts[2]);
	                	
                		this.isloggedin = server.db.login(parts[1], parts[2]);
                		if(this.isloggedin) {
                			this.usuario = parts[1];
                			String s[] = server.db.getSaldo(this.usuario).split(";");
                			
                			this.saldoActual = Integer.parseInt(s[0]);
                			this.saldoInicial = Integer.parseInt(s[1]);
                			
                			this.name = server.db.getNombreCliente(this.usuario);
                			
                			//System.out.println(saldoActual + " " + saldoInicial + " " + name + " " + usuario);
                			
                			server.mostrarMensaje("/connect;Se ha conectado el usuario " + this.usuario);
                			
                			this.dos.writeUTF("Benvingut usuari " + this.usuario);
                			this.dos.writeUTF("/saldo;" + server.db.getInfo(this.usuario));
                			this.server.db.initSaldo();
                			this.dos.writeUTF("/lots;" + server.db.veureGuanyats(server.db.getIdClient(this.usuario)));
                			
                		} else {
                			this.dos.writeUTF("CREDENCIALS INCORRECTES...");
                		}
	                	
	                } else {
	                	if (received.contains("/licitar") && server.subhasta != null) {
	                    	//server.mostrarMensaje(this.name+": "+ received);
	                		String s[] = received.split(" ");
	                		//System.out.println(s[1]);
	                		
	                		String msg = server.subhasta.newLicitacio(this, Integer.parseInt(s[1]));
	                		if(!msg.contains("/cancel"))
	                			server.actualitzarSubhasta(msg); //Actualiza datos en las pantallas
	                		else if(!msg.equals("")){
	                			String a[] = msg.split(";");
	                			this.dos.writeUTF(a[1]);
	                		}
	                	}
	                }
                }
            } catch (SocketException e3) {
				break;
            } catch (EOFException e2) {
            	try {
					this.s.close();
					Server.ar.remove(this);
				} catch (IOException e) {
					e.printStackTrace();
				} 
                break; 
			} catch (IOException e) { 
                  
                e.printStackTrace(); 
            }
        } 
        try
        {
            this.dis.close(); 
            this.dos.close(); 
              
        }catch(IOException e){ 
            e.printStackTrace(); 
        } 
    } 
} 
