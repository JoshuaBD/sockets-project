package tastat;

import java.util.ArrayList;
import java.util.Collections;

import server.DB;

public class DaemonLots extends Thread {
	protected DB db;

	public DaemonLots(DB db) {
		this.db = db;
	}

	public void run() {
		while (true) {
			try {
				ArrayList<Integer> productes = new ArrayList<Integer>();
				productes = db.getProductes();
				Collections.shuffle(productes);
				//System.out.println("------------>"+db.getLotClient());
				if (db.getLotClient() < 5) {
					db.generarLot(productes.get(0));
					sleep(5000);
				} else
					sleep(30000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}