package tastat;

import server.ClientHandler;
import server.DB;

public class Subhasta {
	
	//private static Subhasta subhasta = null;
	public int idLot;
	public int idProducte;
	public int licitacioMax;
	public int preuSortida;
	public int idClient;
	public String usuario = "";
	public String clientMax = "";
	public DB db;
	public boolean acabado;
	
	/*public Subhasta(DB db) {
		db.getNextSubhasta();
		this.db = db;
	}*/
	
	public Subhasta(DB db) {
		this.db = db;
	}
	
	public String getInfo() {
		String s = "ID LOT: "+idLot+"\nID PRODUCTE: " + idProducte + "\nPreu Sortida: " +preuSortida +"\nNom Client: " + clientMax +"\nLicitacio MAX: "+ licitacioMax;
		return s;
	}
	
	public synchronized String newLicitacio(ClientHandler client, int quantitat) {
		if(!this.acabado) {
			if(!clientMax.equals(client.name)) {
				if((licitacioMax + quantitat) <= client.saldoActual) {
					licitacioMax = (licitacioMax + quantitat);
					clientMax = client.name;
					usuario = client.usuario;
					idClient = db.getIdClient(client.usuario);
					
					db.creaLicitacio(idLot, idClient, licitacioMax, true);
					return "Usuari " + client.usuario + " ha pujat la licitació a " + licitacioMax;
				} else {
					//System.out.println( + " " + );
					db.creaLicitacio(idLot, db.getIdClient(client.usuario), (licitacioMax + quantitat), false);
					return "/cancel;No tens suficient saldo...";
				}
			} else {
				return "/cancel;Ja estas guanyant...";
			}
		}
		
		return "";
	}
	
	public String acabarSubhasta() {
		db.actualitzarSubhasta(idLot, idClient, licitacioMax);
		db.restarSaldo(usuario, licitacioMax);
		
		String s = "GUANYADOR: " + clientMax + " IMPORT: " + licitacioMax;
		
		return s;
	}
}
