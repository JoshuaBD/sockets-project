package tastat;

import java.io.IOException;
import java.util.Vector;

import server.ClientHandler;
import server.Server;

public class DaemonSubhasta extends Thread {

	Server server;
	Vector<ClientHandler> ar = new Vector<>();

	public DaemonSubhasta(Server s, Vector<ClientHandler> v) {
		server = s;
		ar = v;
	}

	@Override
	public void run() {
		super.run();

		server.timer = new Timer(server);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		while (true) {
			if (ar.size() >= 2 && server.subhasta == null) {
				int count = 0;
				for (ClientHandler ch : ar) {
					if (ch.isloggedin) {
						count++;
					}
				}

				// System.out.println(count);

				if (count >= 2) {
					server.subhasta = server.db.getNextSubhasta();

					String infoSub = server.subhasta.getInfo();

					for (ClientHandler ch : ar) {
						if (ch.isloggedin) {
							try {
								ch.dos.writeUTF("/subhasta;" + infoSub);
								ch.dos.writeUTF("/new;Comen�a la nova subhasta!");
								
								String s[] = server.db.getSaldo(ch.usuario).split(";");
	                			
	                			ch.saldoActual = Integer.parseInt(s[0]);
	                			ch.saldoInicial = Integer.parseInt(s[1]);
								
								ch.dos.writeUTF("/saldo;" + server.db.getInfo(ch.usuario));
								ch.dos.writeUTF("/lots;" + server.db.veureGuanyats(server.db.getIdClient(ch.usuario)));
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}

				try {
					sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			} else if (server.subhasta != null) {
				if (server.subhasta.acabado) {
					String a = server.subhasta.acabarSubhasta();

					for (ClientHandler ch : ar) {
						if (ch.isloggedin) {
							try {
								ch.dos.writeUTF(a);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}

					server.subhasta = null;

					try {
						sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}

		}
	}
}
